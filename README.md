# superMSAker
super Multiple Sequence Alignment maker


Python 3 script (tested with 3.6) to perform multiple sequence alignment averaging as outlined in:

[Ashkenazy, H., Sela, I., Levy Karin, E., Landan, G. & Pupko, T. Multiple sequence alignment averaging improves phylogeny reconstruction. Syst. Biol. 0, (2018).](https://doi.org/10.1093/sysbio/syy036)

Chooses a random subset of n (def. 20) MSAs from all GUIDANCE2 alternative MSAs, performs rudimentary trimming via the pandas Python package
(take region from first to last no gap columns; optionally, can also run [trimal](http://trimal.cgenomics.org/)), 
and then concatenates them to produce a super-MSA ready for further phylogenetic analysis.


### Usage
`python3 superMSAker.py <best GUIDANCE alignment>  <folder containing alternative MSAs>  <output file>`

### Optional arguments:

```
--trimal    run trimal -automated1 on every MSA
--nlen      number of alternative alignments from GUIDANCE2 to concatenate
```