#!/usr/env/bin python3
'''Create a 'super-MSA' ala GUIDANCE2 but trim for differing length sequences.

Takes default (best) MSA from GUIDANCE2 and random subset (def. n=20) of
alternative MSAs, converts to pandas dataframe and trims left/right from
first/last column where all nucleotides are the same, then concatenates
them all together.
'''

import io
import os
import textwrap
import argparse
import subprocess
import pandas as pd
import numpy as np
from Bio import AlignIO


def choose_msas(path, num):
    '''Randomly select <num> alignments from given path; return file paths.

    Unless specified by the user, will select 20.
    '''
    # Get full path of given dir; list files in dir
    abs_path = os.path.abspath(path)
    all_alns = os.listdir(path)
    # Choose 20; return list of full path names
    sup_alns = np.random.choice(all_alns, num)
    aln_files = ['{}/{}'.format(abs_path, x) for x in sup_alns]
    return(aln_files)


def run_trimal(align):
    '''Trim a MSA using the trimal -automated1 algorithm.
    '''
    # Create trimal command, run and grab output
    cmd = 'trimal -in {} -automated1'.format(align)
    stdout = subprocess.check_output(cmd, shell=True).decode('utf-8')
    # Create StringIO object for AlignIO to read; return
    aln_str = io.StringIO(stdout)
    aln_obj = AlignIO.read(aln_str, 'fasta')
    return(aln_obj)


def trimmer(align):
    '''Trim gapped starts and ends of an MSA.

    Converts to a pandas dataframe, finds first and last all-identical column
    and returns dict of sequences trimmed at those boundaries.
    '''
    # Grab all sequences with corresponding ID's; create dataframe
    raw_seqs = {align[x].id: list(align[x]) for x in range(len(align))}
    df = pd.DataFrame.from_dict(raw_seqs, orient='index')
    # Create new dataframe with no-gap columns
    nogaps = df.loc[:, (df != '-').all(axis=0)]
    # Take first/last column as start/end of 'trimmed' alignment
    start = nogaps.iloc[:, 0].name
    end = nogaps.iloc[:, -1].name
    # Create 'trimmed' alignment
    trim_df = df.iloc[:, start:end].apply(''.join, axis=1)
    trim_seqs = trim_df.to_dict()
    return(trim_seqs)


def concatenate_dicts(one, two):
    '''Merge two dictionaries, joining str values of second to first.

    Returns new dict created from merge.
    '''
    three = {}
    # In case first dictionary is empty (i.e. first alignment)
    # TODO probably unecessary now given I take default MSA first
    if not one:
        three = two
    # Otherwise append sequence strings to those already in first dict
    else:
        for name, seq in one.items():
            three[name] = ''.join([seq, two[name]])
    return(three)


def print_fasta(seq_dict, outfile):
    '''Print concatenated super alignment to user folder.

    Requires break_on_hyphens = False to not wrap on gapped columns.
    '''
    with open(outfile, 'w') as o:
        for name in seq_dict:
            o.write('>{}\n{}\n'.format(
                name,
                textwrap.fill(
                    seq_dict[name],
                    width=80,
                    break_on_hyphens=False)
                ))


def open_alignment(align, trimal_flag):
    '''Process alignment differently depending on if trimal flag is enabled.
    '''
    if trimal_flag:
        return(trimmer(run_trimal(align)))
    else:
        aln = AlignIO.read(align, 'fasta')
        return(trimmer(aln))


def main(args):
    # Read in and trim best default MSA from GUIDANCE2
    print('Reading in default MSA from {}'.format(args.default))
    # Check if user wants to use trimal
    default = open_alignment(args.default, args.trimal)
    # Get list of alternative alignments from given folder
    alt_aligns = choose_msas(args.msa_dir, args.nlen)
    # Iterate all alignments and trim
    supermsa = default
    print('Reading in alternative alignments:')
    for aln in alt_aligns:
        print(aln)
        trim_aln = open_alignment(aln, args.trimal)
        #  print_fasta(trim_aln, '{}_trimmed.msa'.format(aln))
        supermsa = concatenate_dicts(supermsa, trim_aln)
    # Write concated super-MSA to file
    print('Writing super-MSA to {}'.format(args.out))
    print_fasta(supermsa, args.out)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
            description='''
Create 'super-MSA' from random subset (def. n=20) of 400 alternative MSAs.

Trims each individual MSA for length by finding first and last all-same column,
and then concatenates them all to create a super-MSA ala
doi:10.1093/sysbio/syy036

Author: Cameron Gilchrist
Date: 2018-08-08
            ''',
            formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('default', help='Default MSA produced by GUIDANCE2')
    parser.add_argument('msa_dir', help='Folder containing all MSAs')
    parser.add_argument('out', help='Filename to store super-MSA')
    parser.add_argument('--trimal', action='store_true',
                        help='Run trimal -automated1 on every MSA')
    parser.add_argument('--nlen',
                        help='Number of alternative alignments to combine',
                        default=20, type=int)
    args = parser.parse_args()
    main(args)